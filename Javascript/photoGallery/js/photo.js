const gallery=document.getElementById('slider')
const leftar=document.querySelector(".left")
const rightar=document.querySelector(".right")

async function load(){
    const result=await fetch('./data.json')
    const data=await result.json()
    display(data)
}
const display=(data)=>{
    gallery.innerHTML=''
    data.forEach((member)=>{
    const { name, designation, image, project,experience }=member
    const dive=document.createElement('div')
    dive.classList.add('imag')
    dive.innerHTML=`<img src="${image}"><br/>
    <h1>${name}</h1>
    <h2>${designation}</h2>
    <p>Years of experience: ${experience}</p>
    <p>Currently working on Project: ${project}</p>
    `
    gallery.appendChild(dive)
    })
}
leftar.addEventListener("click",()=>{
    document.querySelector("#slider").scrollLeft+=500;
})
rightar.addEventListener("click",()=>{
    document.querySelector("#slider").scrollLeft-=500;
})
load()
