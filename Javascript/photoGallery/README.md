<h1>Assignment-4</h1>
<hr/>

<h2>Develop a gallery view for the company employees. Only 1 image should display big in the center and the left and right arrow should be there for see next employe image. At the bottom user, details should there. Like the name, designation, project and year of experience etc... (Use objects and external JSON file for storing the data and load JSON file while the app will launch).</h2>

<p>A gallery view of employees and a brief overview about them.
</p>

<p>It contains following information about employee: 
<ul>
<li>Name</li>
<li>Project</li>
<li>Image</li>
<li>Post</li>
<li>Experience</li>
</ul>
</p>

<p>It takes data from the json file and displays it
</p>