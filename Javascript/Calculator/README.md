<h1>Assignment-3</h1>
<hr/>

<h2>Create a calculator with basic operations</h2>

<p>Made a calculator using:
<ul>
<li><h6>HTML</h6></li>
<li><h6>CSS</h6></li>
<li><h6>Javascript</h6></li>
</ul>
</p>

<p>It contains basic functionalities like: 
<ul>
<li>Addition</li>
<li>Subtraction</li>
<li>Multiplication</li>
<li>Division</li>
</ul>
</p>

<p>Other than that it has a :
<ul>
<li>Clear Button</li>
<li>Backspace</li>
<li>Can give decimal values</li>
</ul>
</p>