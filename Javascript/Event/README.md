<h1>Assignment-1</h1>
<hr/>

<h2>Create an HTML Page with JavaScript Events Handling</h2>

Made a HTML page that contains three buttons:
<ol>
<li><u><h6>Prompt</h6></u><p>This will open prompt dialog box and asks for a Name</p></li>
<li><u><h6>Confirm</h6><p></u>This opens up a confirmation box</p></li>
<li><u><h6>Alert</h6></u><p>This shows alert box about what all enteries you have given</p></li>
</ol>