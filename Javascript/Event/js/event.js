let promptbox=document.querySelector(".prompt")
let confirmbox=document.querySelector(".confirm")
let alertbox=document.querySelector(".alert")
let firstdiv=document.querySelector(".first")
let seconddiv=document.querySelector(".second")

let confirmtxt='Confirmation Not pressed Yet!'
let prompttxt='Prompt Not preesed Yet!'

promptbox.addEventListener("click",()=>{
    let name=prompt("Enter your name: ","Default Name")
    name==null||name==""?prompttxt="nothing entered":prompttxt=`You have entered: ${name}`
    firstdiv.innerHTML=prompttxt
    firstdiv.classList.add("border")
})

confirmbox.addEventListener("click",()=>{
    confirmtxt="Is this your name: "
    confirm("Is this your name?")?confirmtxt+="Yes":confirmtxt+="No"
    seconddiv.innerHTML=confirmtxt
    seconddiv.classList.add("border")
})

alertbox.addEventListener("click",()=>{
    alert(`${prompttxt}\n${confirmtxt}`)
})