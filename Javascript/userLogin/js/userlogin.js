const usernameregex=/^(?=[a-zA-Z0-9._]{5,20}$)[^_.]/
const passwordregex=/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/
let username=document.querySelector(".username")
let password=document.querySelector(".password")
let regname=document.querySelector(".regname")
let pass=document.querySelector(".pass")
let confpass=document.querySelector(".confpass")
let registered=document.querySelector(".submit")
let storedUsers = localStorage.usersLogin ? JSON.parse(localStorage.usersLogin) : [];

const already=(name)=>{
    for(const prop of storedUsers){
        if(name==prop.email){
            return true
        }
    }
    return false
}

const userRegister=()=>{
    if(regname.value=='' || usernameregex.test(regname.value)==false){
        alert("Please enter a valid user name:\nUsername should be 5 to 20 characters long and should not start with a underscore or a dot ")
    }
    else if(pass.value=='' || passwordregex.test(pass.value)==false){
        alert("Enter a valid password:\npassword should be at least 8 character long with a number and a special character")
    }
    else if(confpass.value!=pass.value){
        alert("Make sure both the passwords are correct")
    }
    else if(already(regname.value)){
        alert("Username already taken")
    }
    else{
        const userData = {
            email: regname.value,
            password: pass.value
        };
        storedUsers.push(userData);
        localStorage.setItem('usersLogin', JSON.stringify(storedUsers));
        window.location="index.html"
    }
}

const login=(name,pass)=> {
    for(const prop of storedUsers){
        if(prop.email==name && prop.password==pass){
            return true          
        }
    }
    return false
}
const loginUser=()=>{
    if(login(username.value,password.value)){
        window.location.assign(window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/Javascript/photoGallery/")//http://127.0.0.1:5500/Javascript/photoGallery/
    }
    else{
        alert("wrong credentials")
    }
}

registered.addEventListener("click",userRegister)

